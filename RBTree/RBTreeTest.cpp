#include "RBTree.h"

template<class T>
void insertAndPrint(RBTree<T>* tree, T value){

	std::cout << "Inserting " << value << std::endl;
	tree->insert(value);
	tree->printInOrder(); 
	tree->printLevelOrder(); 
	std::cout << std::endl;
	
}

template<class T>
void deleteAndPrint(RBTree<T>* tree, T value){

	std::cout << "Deleting " << value << std::endl;
	tree->deleteByVal(value);
	tree->printInOrder(); 
	tree->printLevelOrder(); 
	std::cout << std::endl;
	
}	

int main(int argc, char** argv) { 

  RBTree<int> tree; 
  
  // Showing the results of insertion and deletion
  // Insert: 20, 10, 30, 15, 17, 12, 35, 25, 13, and 9. 
	insertAndPrint(&tree, 20);
	insertAndPrint(&tree, 10);
	insertAndPrint(&tree, 30);
	insertAndPrint(&tree, 15);
	insertAndPrint(&tree, 17);
	insertAndPrint(&tree, 12);
	insertAndPrint(&tree, 35);
	insertAndPrint(&tree, 25);
	insertAndPrint(&tree, 13);
	insertAndPrint(&tree, 9);
	
  // Delete 30, 20, 25, 35, 17, 12, 9, 13, 10, and 15
	deleteAndPrint(&tree, 30);
	deleteAndPrint(&tree, 20);
	deleteAndPrint(&tree, 25);
	deleteAndPrint(&tree, 35);
	deleteAndPrint(&tree, 17);
	deleteAndPrint(&tree, 12);
	deleteAndPrint(&tree, 9);
	deleteAndPrint(&tree, 13);
	deleteAndPrint(&tree, 10);
	deleteAndPrint(&tree, 15);
  
  return 0; 
} 