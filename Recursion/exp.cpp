/**********************************************
* File: exp.cpp
* Author: Matthew Morrison
* Email: matt.morrison@nd.edu
* 
* The main driver for the exponential recursive
* solver 
**********************************************/
#include "Supp.h"
#include "Recurse.h"

int main(int argc, char** argv){
	
	std::cout << exponential(getArgv1Num(argc, argv), 50) << std::endl;
	
	return 0;
}
